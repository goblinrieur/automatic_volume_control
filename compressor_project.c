/*
 * compressor_project.c
 *
 * Implements a simple audio compressor using a single analog input representing the peak
 * amplitude of the input audio waveform.  Controls output level via a digital potentiometer.
 *
 * Created: 1/3/2012 6:36:10 PM
 * Author: Mike Kleinigger
 */ 
#include <avr/io.h>
#define cbi(reg, bit) reg &= ~(1<<(bit))	//Clears the corresponding bit in register reg
#define sbi(reg, bit) reg |= (1<<(bit)) 	//Sets the corresponding bit in register reg
//Define ports and pins for the LED bar graph (for displaying either peak voltage or pot wiper position)
#define LPORT1	PORTD
#define LPORT2	PORTD
#define LPORT3	PORTB
#define LPORT4	PORTB
#define LPORT5	PORTB
#define LPORT6	PORTC
#define LPORT7	PORTC
#define LPORT8	PORTC
#define LPORT9	PORTC
#define LPORT10	PORTC
#define LPIN1	6
#define LPIN2	7
#define LPIN3	0
#define LPIN4	1
#define LPIN5	2
#define LPIN6	1
#define LPIN7	2
#define LPIN8	3
#define LPIN9	4
#define LPIN10	5
//Define the chip select output port and pin
#define CS_PORT	PORTD
#define CS_PIN	0
void set_lights(uint8_t val){
	//This routine controls the 10 output lines needed to drive the LED bar graph
	//(there are undoubtedly simpler ways to do this, but when space and performance
	//aren't issues, I tend to go with the dumb, yet obvious, solutions.)
	switch (val)
	{
	case 0:
		cbi(LPORT1,LPIN1);
		cbi(LPORT2,LPIN2);
		cbi(LPORT3,LPIN3);
		cbi(LPORT4,LPIN4);
		cbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 1:
		sbi(LPORT1,LPIN1);
		cbi(LPORT2,LPIN2);
		cbi(LPORT3,LPIN3);
		cbi(LPORT4,LPIN4);
		cbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 2:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		cbi(LPORT3,LPIN3);
		cbi(LPORT4,LPIN4);
		cbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 3:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		cbi(LPORT4,LPIN4);
		cbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 4:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		cbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 5:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		cbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 6:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		cbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 7:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		sbi(LPORT7,LPIN7);
		cbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 8:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		sbi(LPORT7,LPIN7);
		sbi(LPORT8,LPIN8);
		cbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 9:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		sbi(LPORT7,LPIN7);
		sbi(LPORT8,LPIN8);
		sbi(LPORT9,LPIN9);
		cbi(LPORT10,LPIN10);
		break;
	case 10:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		sbi(LPORT7,LPIN7);
		sbi(LPORT8,LPIN8);
		sbi(LPORT9,LPIN9);
		sbi(LPORT10,LPIN10);
		break;
	default:
		sbi(LPORT1,LPIN1);
		sbi(LPORT2,LPIN2);
		sbi(LPORT3,LPIN3);
		sbi(LPORT4,LPIN4);
		sbi(LPORT5,LPIN5);
		sbi(LPORT6,LPIN6);
		sbi(LPORT7,LPIN7);
		sbi(LPORT8,LPIN8);
		sbi(LPORT9,LPIN9);
		sbi(LPORT10,LPIN10);
		break;
	}
	return;
}
void delay_ms(long x){
	//Implements a simple 1ms delay based on repeating a loop multiple times
	//(Note that this is NOT ACCURATE, but timing isn't very critical here.)
	int y;
	while(x--){
		y=280;
		while(y--);
	}
	return;
}
uint8_t interpolate(int filtered_value){
	//Interpolates between a set of points emperically determined to correlate
	//our input ADC values with the most-suitable wiper positions (it's basically an exponential curve)
	if (filtered_value < 100) return 255;
	if (filtered_value >=100 && filtered_value < 200) return (255-(filtered_value-100)*(255-156)/100);
	if (filtered_value >=200 && filtered_value < 300) return (156-(filtered_value-200)*(156-110)/200);
	if (filtered_value >=300 && filtered_value < 400) return (110-(filtered_value-300)*(110-82)/300);
	if (filtered_value >=400 && filtered_value < 600) return (82-(filtered_value-400)*(82-52)/400);
	if (filtered_value >=600 && filtered_value < 900) return (52-(filtered_value-600)*(52-20)/600);
	if (filtered_value >=900 && filtered_value < 1000) return (20-(filtered_value-900)*(20-10)/900);
	if (filtered_value >= 1000) return 10;
}
int main(void)
{
	//Setup variables
	int adc_result;
	int filtered_adc = 255;
	uint8_t wiper_position;
	uint8_t wiper_position_last;
	int delta;
	//Configure ports for input/output
	DDRB = 0b00101111;
	DDRC = 0b00111110;
	DDRD = 0b11000001;
	//Enable pull-up on PORTB (for button input for switching between LED display modes)
	sbi(PORTB,7);
	//Configure the ADC
	ADCSRA = 0xA6;
	ADCSRB = 0xF8;
	ADMUX = 0x40;
	DIDR0 = 0x01;
	//Enable the SPI interface in master mode, with CLK/16
	SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
	//This is the main program loop, which runs continuously
    while(1)
    {
		//Begin next A->D conversion
		ADCSRA|=0x40;
		//Wait for conversion to complete
		while (!(ADCSRA & (1 << ADIF)));
		//Read ADC result
		adc_result = ADCW;
		//Compute the filtered ADC value
		if (adc_result > filtered_adc){
			//Input audio level is higher than our filtered value, so increase the filtered value (fairly rapidly)
			delta = (adc_result - filtered_adc)/40;
			//If division result is zero, we still want to add something
			if (delta == 0) delta = 1;
			filtered_adc += delta;
			if (filtered_adc > 1024) filtered_adc = 1024;
		}else{
			if (adc_result != filtered_adc){
				//Input audio level is lower than our filtered value, so decrease the filtered value (fairly slowly)
				delta = (filtered_adc - adc_result)/240;
				//If division result is zero, we still want to subtract something
				if (delta == 0) delta = 1;
				filtered_adc -= delta;
				if (filtered_adc < 0) filtered_adc = 0;
			}				
		}
		//Perform interpolation to find output wiper position
		wiper_position = interpolate(filtered_adc);
		//Only send new wiper position to potentiometer if it has changed since the last iteration
		if (wiper_position != wiper_position_last){
			//Set CS pin low to trigger the pot to receive data
			cbi(CS_PORT,CS_PIN);
			//Place the command byte into the SPI output register (commands both pots to the same value)
			SPDR = 0b11011111;
			//Wait for transmit to complete
			while(!(SPSR & (1 << SPIF)));
			//Place the wiper position byte into the SPI output register
			SPDR = 255-wiper_position;
			//Wait for transmit to complete
			while(!(SPSR & (1 << SPIF)));
			//Set CS pin high again to trigger the pot to update
			sbi(CS_PORT,CS_PIN);
		}
		//Update the value of the last wiper position
		wiper_position_last = wiper_position;
		//Handle what to display on the LED bar graph
		if (bit_is_set(PINB,7)){
			//Button is not pressed, just pulled up internally, so display instantaneous ADC reading
			set_lights(adc_result/90);
		}else{
			//Button is pressed, display wiper position on LED bar graph
			set_lights(wiper_position/25);
		}			
		//Wait 50ms for the next iteration
		delay_ms(50);
    }
}